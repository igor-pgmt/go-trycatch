package tryCatch

import "time"

type TryCatchRetry struct {
	*TryCatch
	*Errors
}

type Errors struct {
	retriesNum uint64
	errorPause time.Duration
}

func NewTryCatchRetry(
	try func() error,
	catch func(error) error,
	finally func(error) error,
	retriesNum uint64,
	errorPause time.Duration,
) *TryCatchRetry {
	return &TryCatchRetry{
		&TryCatch{try, catch, finally},
		&Errors{retriesNum, errorPause},
	}
}

func (tc *TryCatchRetry) Do() (err error) {

	for retry := uint64(0); retry < tc.retriesNum; retry++ {

		err = tc.Try()

		if err != nil {
			if tc.Catch != nil {
				err = tc.Catch(err)
			}
			time.Sleep(tc.errorPause)
			continue
		}
		break

	}

	if tc.Finally != nil {
		err = tc.Finally(err)
	}

	return
}
