package tryCatch

type TryCatch struct {
	Try     func() error
	Catch   func(error) error
	Finally func(error) error
}

func NewTryCatch(
	try func() error,
	catch func(error) error,
	finally func(error) error,
) *TryCatch {
	return &TryCatch{try, catch, finally}
}

func (tc *TryCatch) Do() (err error) {

	err = tc.Try()

	if err != nil {
		if tc.Catch != nil {
			err = tc.Catch(err)
		}
	}

	if tc.Finally != nil {
		err = tc.Finally(err)
	}

	return
}
